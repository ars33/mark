// CSS Color Names
// Compiled by @bobspace.
//
// A javascript array containing all of the color names listed in the CSS Spec.
// The full list can be found here: http://www.w3schools.com/cssref/css_colornames.asp
// Use it as you please, 'cuz you can't, like, own a color, man.

var CSS_COLOR_NAMES = ["AliceBlue", "AntiqueWhite", "Aqua", "Aquamarine", "Azure", "Beige", "Bisque", "Black", "BlanchedAlmond", "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse", "Chocolate", "Coral", "CornflowerBlue", "Cornsilk", "Crimson", "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "Darkorange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", "DimGray", "DimGrey", "DodgerBlue", "FireBrick", "FloralWhite", "ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite", "Gold", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow", "HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan", "LightGoldenRodYellow", "LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSlateGrey", "LightSteelBlue", "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", "OldLace", "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum", "PowderBlue", "Purple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet", "Wheat", "White", "WhiteSmoke", "Yellow", "YellowGreen"];

for (var i = 0; i < CSS_COLOR_NAMES.length; i++) {
  $('.box').append("<div class='t' style='background:" + CSS_COLOR_NAMES[i] + "'></div>")
  $('.boxb').append("<div class='t' style='background:" + CSS_COLOR_NAMES[i] + "'></div>")
}

var CAN = false;
$('body').on('mousedown', (e) => {
  CAN = true;
})

$('body').on('mouseup', (e) => {
  CAN = false;
})
var content = [
  'hi',
  "I'm",
  'mark',
  'and',
  'I',
  'draw',
  '3 minutes',
  'maza-faka',
  'adaptive',
  'realtime',
  'portraits',
  'on 🇧🇪',
  'Yami-Ichi'
]
var tag = function() {
  var ar = [
    'p',
    'span',
    'h1',
    'h2',
    'h3',
    'h4',
    'h5',
    'h6',
    'input',
    'textarea',
    'button'
  ]

  return ar[Math.floor(Math.random() * ar.length)];
}

var randc = function(){
  return CSS_COLOR_NAMES[Math.floor(Math.random() * CSS_COLOR_NAMES.length)]
}

var tp = function(){
  if(Math.random()>.8){
    return " type='" + ['checkbox','ratio'][Math.floor(Math.random()*2)]+"' "
  }else{
    return ' '
  }
}

var randu = function(X , sensivity ){
  var x1 = Math.random() > .5 ? -1 : 1;
  return X + (x1 * Math.floor( Math.random() * (sensivity||10) ) );
}

var lazyLayout = _.throttle(function(e) {
  if (CAN) {
    var x = randu(e.clientX, 100),
      y = randu(e.clientY, 100);
    var t = tag();
    var c = content.splice(0, 1)[0];
    if (c) {
      $('body').append("<" + t + " " + tp() + "  value='" + c + "' style='background:white;position:absolute;left:" + x + "px;top:" + y + "px' >" + c + "</" + t + ">")
    }
  }
}, 500, true);
$('body').on('mousemove', lazyLayout)
