var insertAltVideo = function(video) {
  // insert alternate video if getUserMedia not available
  if (supports_video()) {
    if (supports_webm_video()) {
      video.src = "./media/cap12_edit.webm";
    } else if (supports_h264_baseline_video()) {
      video.src = "./media/cap12_edit.mp4";
    } else {
      return false;
    }
    return true;
  } else return false;
}

function adjustVideoProportions() {
  // resize overlay and video if proportions of video are not 4:3
  // keep same height, just change width
  var proportion = vid.videoWidth/vid.videoHeight;
  vid_width = Math.round(vid_height * proportion);
  vid.width = vid_width;
  overlay.width = vid_width;
}

function gumSuccess( stream ) {
  // add camera stream if getUserMedia succeeded
  if ("srcObject" in vid) {
    vid.srcObject = stream;
  } else {
    vid.src = (window.URL && window.URL.createObjectURL(stream));
  }
  vid.onloadedmetadata = function() {
    adjustVideoProportions();
    vid.play();
  }
  vid.onresize = function() {
    adjustVideoProportions();
    if (trackingStarted) {
      ctrack.stop();
      ctrack.reset();
      ctrack.start(vid);
    }
  }
}

function gumFail() {
  // fall back to video if getUserMedia failed
  insertAltVideo(vid);
  document.getElementById('gum').className = "hide";
  document.getElementById('nogum').className = "nohide";
  alert("There was some problem trying to fetch video from your webcam, using a fallback video instead.");
}

navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
window.URL = window.URL || window.webkitURL || window.msURL || window.mozURL;

// set up video
if (navigator.mediaDevices) {
  navigator.mediaDevices.getUserMedia({video : true}).then(gumSuccess).catch(gumFail);
} else if (navigator.getUserMedia) {
  navigator.getUserMedia({video : true}, gumSuccess, gumFail);
} else {
  insertAltVideo(vid);
  document.getElementById('gum').className = "hide";
  document.getElementById('nogum').className = "nohide";
  alert("Your browser does not seem to support getUserMedia, using a fallback video instead.");
}

function startVideo() {
  // start video
  vid.play();
  // start tracking
  ctrack.start(vid);
  trackingStarted = true;
  // start loop to draw face
  drawLoop();
}



function modes(m) {
  console.log(m);
  switch (m) {
    case 1:
      $('body').css('background', pick(['lightpink', 'yellow' , 'blue'])[0] )
      setTimeout(function(){
        $('body').css('background', 'white')
        window.mode = false;
      }, 1000);
      break;
    case 2:
      $('#content').show()
      setTimeout(function(){
        $('#content').hide()
        window.mode = false;
      }, 5000);
      break;
    case 3:
      var deg = 0;
      window.inter = setInterval(function(){
        deg+=1;
        $('body').attr('style',
        `
          transform: rotate(${deg}deg)
        `)
      } , 10)
      setTimeout(function(){
        clearInterval(window.inter);
        $('body').attr('style', '');
        window.mode = false;
      }, 7000);
      break;
    case 4:
      $('body').css('font-family', pick(['Gill Sans', 'Helvetica', 'Comic sans', 'Pragmatica', 'SF Pro', 'DIN'])[0])
      setTimeout(function(){
        $('body').css('font-family', 'Times New Roman');
        window.mode = false;
      }, 5000);
      break;
      case 5:
        var link = document.createElement('a');
        link.setAttribute('href','./js/funcs.js');
        link.setAttribute('download','download');
        onload=link.click();
        setTimeout(function(){
          window.mode = false;
        }, 5000);
        break;
        // case 6:
        //   $('body').css(`-webkit-animation`,`color var(--duration) linear infinite;`);
        //   $('body').css(`animation`, `color var(--duration) linear infinite;`);
        //   setTimeout(function(){
        //     $('body').css(`-webkit-animation`,`none`);
        //     $('body').css(`animation`, `none`);
        //     window.mode = false;
        //   }, 5000);
        //   break;
          case 6:
            $('body').css('filter', 'blur(4px)')
            setTimeout(function(){
              $('body').css('filter', 'none')
              window.mode = false;
            }, 5000);
            break;
            case 7:

              var more = ['hello', 'Guest', 'Its', 'sound', 'about', 'your', 'live', 'how','are','you' ];

              window.inter = setInterval(function(){
                if(more.length){
                  window.history.pushState('page2', 'Title', more.splice(0,1)[0] );
                }else{
                  clearInterval(window.inter);
                  window.mode = false;
                }
              }, 600)
              break;
              case 8:
                alert("You just a click!")
                setTimeout(function(){
                  window.mode = false;
                }, 100);
                break;
              case 9:
                var win = window.open("http://localhost:3000", "Yami", "width=700,height=400,_blank");
                setTimeout(function(){
                  win.close();
                  window.mode = false;
                }, 10000);
                break;

    default:
      window.mode  = false;
  }
}
